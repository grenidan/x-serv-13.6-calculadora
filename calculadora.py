#!/usr/bin/python3

import sys
from sys import argv


if len(sys.argv) != 4:
	sys.exit("Invalid number of arguments")

def usage():
	sys.exit("Usage: python3 calculadora.py operacion operando1 operando2")

operacion = argv[1]

try:
	operando1 = float(sys.argv[2])
	operando2 = float(sys.argv[3])
except ValueError:
	usage()

if operacion == 'sumar':
	print(operando1 + operando2)
elif operacion == 'restar':
	print(operando1 - operando2)
elif operacion == 'multiplicar':
	print(operando1 * operando2)
elif operacion == 'dividir':
	try:
		print(operando1 / operando2)
	except ZeroDivisionError:
		print("No se puede dividir entre 0")
else:
	usage()